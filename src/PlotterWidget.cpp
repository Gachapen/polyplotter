#include "PlotterWidget.h"

#include <QPainter>
#include <QWheelEvent>
#include <QMimeData>
#include <QMessageBox>

PlotterWidget::PlotterWidget(QWidget *parent) :
	QWidget(parent),
	scale(1.0f),
	panning(false),
	plotting(false)
{
}

void PlotterWidget::setImage(const QImage& image)
{
	this->image = image;
}

QString PlotterWidget::generateWkt(bool clockWise, bool close)
{
	if (this->polygon.size() < 3)
	{
		return QString();
	}

	QString wkt;
	wkt += "POLYGON (";

	for (int i = 0; i < this->polygon.size(); i++)
	{
		const QPointF& point = this->polygon[i];
		wkt += QString::number(point.x()) + " " + QString::number(point.y());

		if (i < this->polygon.size() - 1)
		{
			wkt += ", ";
		}
	}

	if (close == true)
	{
		const QPointF& firstPoint = this->polygon.first();
		wkt += ", " + QString::number(firstPoint.x()) + " " + QString::number(firstPoint.y());
	}

	wkt += ")";

	return wkt;
}

void PlotterWidget::paintEvent(QPaintEvent* paintEvent)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing, true);

	painter.translate(this->currentPanning);
	painter.translate(this->rect().center());
	painter.scale(this->scale, this->scale);

	if (this->image.isNull() == false)
	{
		QRect imageRect = this->image.rect();

		painter.save();
		painter.translate(-imageRect.center());
		painter.drawImage(imageRect, this->image);
		painter.restore();
	}

	if (this->polygon.size() >= 2)
	{
		painter.setBrush(QBrush(QColor(30, 30, 255, 40)));
		painter.drawPolygon(this->polygon);
	}

	paintEvent->accept();
}

void PlotterWidget::wheelEvent(QWheelEvent* event)
{
	QPoint numPixels = event->pixelDelta();
	QPoint numDegrees = event->angleDelta() / 8;

	if (numPixels.isNull() == false)
	{
		this->zoom(numPixels.y());
	}
	else if (!numDegrees.isNull())
	{
		QPoint numSteps = numDegrees / 15;
		this->zoom(numSteps.y());
	}

	event->accept();
}

void PlotterWidget::mousePressEvent(QMouseEvent* event)
{
	if (event->button() == Qt::MouseButton::LeftButton)
	{
		this->polygon << this->translateToWorld(event->pos());

		this->update();

		this->plotting = true;
	}
	else if (event->button() == Qt::MouseButton::RightButton && this->panning == false)
	{
		this->panning = true;
		this->previousMousePosition = event->pos();
	}

	event->accept();
}

void PlotterWidget::mouseReleaseEvent(QMouseEvent* event)
{
	if (event->button() == Qt::MouseButton::RightButton && this->panning == true)
	{
		this->panning = false;
	}

	if (event->button() == Qt::MouseButton::LeftButton && this->plotting == true)
	{
		this->plotting = false;
	}

	event->accept();
}

void PlotterWidget::mouseMoveEvent(QMouseEvent* event)
{
	if (this->panning == true)
	{
		this->currentPanning += event->pos() - this->previousMousePosition;
		this->previousMousePosition = event->pos();

		this->update();
	}

	if (this->plotting == true)
	{
		this->polygon.back() = this->translateToWorld(event->pos());
		this->update();
	}

	event->accept();
}

void PlotterWidget::dragEnterEvent(QDragEnterEvent* event)
{
	if (event->mimeData()->hasImage() == true || event->mimeData()->hasUrls() == true)
	{
		event->acceptProposedAction();
	}
}

void PlotterWidget::dropEvent(QDropEvent* event)
{
	QImage image;

	if (event->mimeData()->hasImage() == true)
	{
		image = event->mimeData()->imageData().value<QImage>();
	}
	else if (event->mimeData()->hasUrls() == true)
	{
		// We only want one image, so take the first.
		QUrl url = event->mimeData()->urls().first();

		if (url.isLocalFile() == true)
		{
			image.load(url.path());
		}
	}

	if (image.isNull() == false)
	{
		this->setImage(image);
		this->update();
		event->acceptProposedAction();
	}
	else
	{
		QMessageBox::information(this, tr("Could not open image"), tr("The file dragged here could not be loaded. Only images are accepted."));
	}
}

void PlotterWidget::zoom(int relativeZoom)
{
	this->scale += (float)relativeZoom / 50.0f;

	if (this->scale < this->MIN_SCALE)
	{
		this->scale = this->MIN_SCALE;
	}

	this->update();
}

QPointF PlotterWidget::translateToWorld(const QPointF& point) const
{
	QPointF translation = -this->currentPanning - this->rect().center();
	float scale(1.0f / this->scale);

	return (point + translation) * scale;
}
