#include "PolyplotterWindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    PolyplotterWindow w;
    w.show();

    return a.exec();
}
