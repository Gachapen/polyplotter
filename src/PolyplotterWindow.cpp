#include "PolyplotterWindow.h"

#include <QFileDialog>
#include <QMessageBox>

#include "ExportView.h"
#include "ui_PolyplotterWindow.h"

PolyplotterWindow::PolyplotterWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::PolyplotterWindow)
{
	this->ui->setupUi(this);

	this->connect(this->ui->actionOpenImage, SIGNAL(triggered()), this, SLOT(openImage()));
	this->connect(this->ui->viewExportButton, SIGNAL(clicked()), this, SLOT(viewExport()));
}

PolyplotterWindow::~PolyplotterWindow()
{
	delete this->ui;
}

void PolyplotterWindow::openImage()
{
	QString imageFileName = QFileDialog::getOpenFileName(this, tr("Open Image"));

	if (imageFileName.isEmpty() == false)
	{
		QImage image(imageFileName);

		if (image.isNull() == true)
		{
			QMessageBox::information(this, tr("Could not open image"), tr("Could not successfully load image %1.").arg(imageFileName));
		}
		else
		{
			this->ui->plotterWidget->setImage(image);
		}
	}
}

void PolyplotterWindow::viewExport()
{
	bool close = this->ui->closePolygonCheckBox->isChecked();
	bool clockWise = !this->ui->ccwCheckBox->isChecked();
	QString exportText;

	if (this->ui->wktRadioButton->isChecked() == true)
	{
		exportText = this->ui->plotterWidget->generateWkt(clockWise, close);
	}

	ExportView exportView(this);
	exportView.setExportedText(exportText);
	exportView.exec();
}
