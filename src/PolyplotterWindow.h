#ifndef POLYPLOTTERWINDOW_H
#define POLYPLOTTERWINDOW_H

#include <QMainWindow>

namespace Ui {
class PolyplotterWindow;
}

class PolyplotterWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit PolyplotterWindow(QWidget *parent = 0);
	~PolyplotterWindow();

private:
	Ui::PolyplotterWindow *ui;

public slots:
	void openImage();
	void viewExport();
};

#endif // POLYPLOTTERWINDOW_H
