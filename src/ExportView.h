#ifndef EXPORTVIEW_H
#define EXPORTVIEW_H

#include <QDialog>

namespace Ui {
class ExportView;
}

class ExportView : public QDialog
{
	Q_OBJECT

public:
	explicit ExportView(QWidget *parent = 0);
	~ExportView();

	void setExportedText(const QString& text);

private:
	Ui::ExportView *ui;
};

#endif // EXPORTVIEW_H
