#include "ExportView.h"
#include "ui_ExportView.h"

ExportView::ExportView(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::ExportView)
{
	this->ui->setupUi(this);
}

ExportView::~ExportView()
{
	delete this->ui;
}

void ExportView::setExportedText(const QString& text)
{
	this->ui->textView->setText(text);
}
