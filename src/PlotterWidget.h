#ifndef PLOTTERWIDGET_H
#define PLOTTERWIDGET_H

#include <QWidget>
#include <QImage>
#include <QPolygonF>

class PlotterWidget : public QWidget
{
	Q_OBJECT
public:
	explicit PlotterWidget(QWidget* parent = 0);

	void setImage(const QImage& image);

	QString generateWkt(bool clockWise, bool close);

protected:
	void paintEvent(QPaintEvent* paintEvent) override;
	void wheelEvent(QWheelEvent* event) override;
	void mousePressEvent(QMouseEvent* event) override;
	void mouseReleaseEvent(QMouseEvent* event) override;
	void mouseMoveEvent(QMouseEvent* event) override;
	void dragEnterEvent(QDragEnterEvent* event) override;
	void dropEvent(QDropEvent* event) override;

private:
	void zoom(int relativeZoom);
	QPointF translateToWorld(const QPointF& point) const;

	QImage image;
	QPolygonF polygon;

	const float MIN_SCALE = 0.1f;
	float scale;
	QPoint currentPanning;

	bool panning;
	bool plotting;
	QPoint previousMousePosition;

signals:

public slots:

};

#endif // PLOTTERWIDGET_H
