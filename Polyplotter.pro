#-------------------------------------------------
#
# Project created by QtCreator 2014-01-31T18:55:17
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Polyplotter
TEMPLATE = app


SOURCES	+= src/main.cpp\
        src/PolyplotterWindow.cpp \
	src/PlotterWidget.cpp \
	src/ExportView.cpp

HEADERS	+= src/PolyplotterWindow.h \
	src/PlotterWidget.h \
	src/ExportView.h

FORMS	+= ui/PolyplotterWindow.ui \
	ui/ExportView.ui

QMAKE_CXXFLAGS += -std=c++11

DESTDIR = build
OBJECTS_DIR = build
UI_DIR = gen
MOC_DIR = gen
RCC_DIR = gen
