# README #
## Description ##
GUI program to plot a polygon and export it in several formats.
Currently supported formats are:
* WKT (Well-known Text)

## Issues ##
Issues can be seen and created at the project's [Jira site](https://jira.daidata.net/browse/POLY/).

## Building ##
### Requirements ###
* Qt 5 libs, or better.
* qmake

### How To ###
Just run qmake in the top directory and it should create a "build/" directory with the built program.

## Contribution Guidelines ##
There are multiple ways you can contribute:
* Add issues to the Jira project.
* Make a pull request and I'll review it.
* Contact me over mail: mbvett@gmail.com.

## License ##
Currently none. I'm thinking about GPLv3